# Config Manager

This library intends to load different types of configuration resources, such as:

- JSON files
- YAML files
- INI files
- .ENV files

This library also loads files inside folders.

A very common example is when you have a master-JSON file to define the defaults in your application, and want to merge with a .env file, overriding some values.


## How to use...

```php
$cfg = new ConfigManager();

// Loads the myFile-01.json contents
$cfg->loadJSON(__DIR__.'/myFile-01.json');

// Loads the myFile-02.json contents, overriding the already existent keys
$cfg->loadJSON(__DIR__.'/myFile-02.json');

// Getting the CONFIG-KEY value
$value = $cfg->get('CONFIG-KEY');

// Getting a value that is not defined on those files - return the default
$default = time();
$expireAt = $cfg->get('EXPIRE_AT', $default);


```

Loading JSON format file
```php

// Loading a single file
$cfg->loadJSON('path-of-JSON-file');

// Loading all files on a folder
$cfg->loadJsonFolder('folder-of-JSON-files');

```


Loading .env format file
```php
// Loading a single file
$cfg->loadEnv('path-of-env-file');

// Loading all files on a folder
$cfg->loadEnvFolder('folder-of-env-files');

```



Loading YAML format file
```php
// Loading a single file
$cfg->loadYaml('path-of-yaml-file');

// Loading all files on a folder
$cfg->loadYamlFolder('folder-of-yaml-files');

```


Loading INI format file
```php
// Loading a single file
$cfg->loadIni('path-of-ini-file');

// Loading all files on a folder
$cfg->loadIniFolder('folder-of-ini-files');

```


## Using multi-level keys

Sometimes you need get/set some values inside of a nested structure. You can set/get key-values using an array as key, as stated in the example below:

```php
$cfg = new ConfigManager();

$networkConfig = [
    'timeout' => 500,
    'host' => 10.20.30.40
];

// Apply the $networkConfig on $cfg
$cfg->set('network', $networkConfig);

// Overriding programatically the 'timeout' key-value
$cfg->set(['network', 'timeout'],1000);

//...

// If the timeout exists inside of 'network', then use it. Otherwise, return the default (700) value.
$cfg->get(['network', 'timeout'],700);

```
