<?php

use PHPUnit\Framework\TestCase;
use \harpya\config_manager\ConfigManager;

class ConfigManagerFoldersTest extends TestCase
{
    public function testLoadFolderDoesNotExists()
    {
        $cfg = new ConfigManager();
        $this->expectException(\harpya\config_manager\exceptions\ConfigException::class);
        $this->expectExceptionMessageRegExp('/Folder (.*) does not exists/');
        $cfg->loadEnvFolder(__DIR__.'/folders/inexistent');
    }


    public function testLoadFolderEnv()
    {
        $cfg = new ConfigManager();
        $cfg->loadEnvFolder(__DIR__.'/folders/env');
        $this->assertEquals('20', $cfg->get('CODE'));
        $this->assertEquals('1', $cfg->get('FIRST'));
        $this->assertEquals('1', $cfg->get('SECOND'));
        $this->assertEquals('1', $cfg->get('THIRD'));
    }

    public function testLoadFolderIni()
    {
        $cfg = new ConfigManager();
        $cfg->loadIniFolder(__DIR__.'/folders/ini');
        $this->assertEquals('20', $cfg->get(['default','CODE']));
        $default = $cfg->get('default');
        $this->assertTrue(is_array($default));
        $this->assertArrayHasKey('FIRST', $default);
        
        $this->assertEquals('1', $default['FIRST']);

        $this->assertEquals('1', $cfg->get(['default','FIRST']));
        $this->assertEquals('1', $cfg->get(['default','SECOND']));
        $this->assertEquals('1', $cfg->get(['default','THIRD']));
    }

    public function testLoadFolderJson()
    {
        $cfg = new ConfigManager();
        $cfg->loadJsonFolder(__DIR__.'/folders/json');
        $this->assertEquals('20', $cfg->get(['default','CODE']));
        $default = $cfg->get('default');
        $this->assertTrue(is_array($default));
        $this->assertArrayHasKey('FIRST', $default);
        
        $this->assertEquals('1', $default['FIRST']);

        $this->assertEquals('1', $cfg->get(['default','FIRST']));
        $this->assertEquals('1', $cfg->get(['default','SECOND']));
        $this->assertEquals('1', $cfg->get(['default','THIRD']));
    }

    public function testLoadFolderYaml()
    {
        $cfg = new ConfigManager();
        $cfg->loadYamlFolder(__DIR__.'/folders/yaml');
        
        $default = $cfg->get('default');
        $this->assertTrue(is_array($default));
        $this->assertArrayHasKey('FIRST', $default);
        
        $this->assertEquals('1', $default['FIRST']);

        $this->assertEquals('1', $cfg->get(['default','FIRST']));
        $this->assertEquals('1', $cfg->get(['default','SECOND']));
        $this->assertEquals('1', $cfg->get(['default','THIRD']));
    }
}
