<?php

namespace harpya\config_manager\adapters;

use harpya\config_manager\exceptions\ConfigException;

class JsonFileAdapter extends BaseAdapter
{
    public function load(string $fileName)
    {
        $this->checkFileExists($fileName);
        
        $json = file_get_contents($fileName);

        $arr = \json_decode($json, true);

        if (!is_array($arr)) {
            throw new ConfigException("Invalid JSON contents in $fileName");
        }

        $this->getContext()->mergeConfig($arr);
    }
}
