<?php

namespace harpya\config_manager\adapters;

use harpya\config_manager\exceptions\ConfigException;
use Symfony\Component\Yaml\Yaml;

class YamlFileAdapter extends BaseAdapter
{
    public function load(string $fileName)
    {
        $this->checkFileExists($fileName);
        $arr = Yaml::parseFile($fileName);

        if (!is_array($arr)) {
            throw new ConfigException("Invalid YAML contents in $fileName");
        }

        $this->getContext()->mergeConfig($arr);
    }
}
