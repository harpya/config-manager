<?php

use harpya\config_manager\adapters\Core;
use harpya\config_manager\adapters\EnvFileAdapter;
use harpya\config_manager\adapters\IniFileAdapter;
use harpya\config_manager\adapters\JsonFileAdapter;
use harpya\config_manager\Constants;
use harpya\config_manager\exceptions\ConfigException;
use PHPUnit\Framework\TestCase;

class CoreTest extends TestCase
{
    protected function getCoreObj()
    {
        $obj = new class {
            use Core;

            public function getAdapter($type)
            {
                return $this->getAdapters($type);
            }
        };

        return $obj;
    }

    public function testGetAdapterJson()
    {
        $adapter = $this->getCoreObj()->getAdapter(Constants::ADAPTER_JSON);
        $this->assertEquals(JsonFileAdapter::class, get_class($adapter));
    }

    public function testGetAdapterEnv()
    {
        $adapter = $this->getCoreObj()->getAdapter(Constants::ADAPTER_ENV);
        $this->assertEquals(EnvFileAdapter::class, get_class($adapter));
    }

    public function testGetAdapterIni()
    {
        $adapter = $this->getCoreObj()->getAdapter(Constants::ADAPTER_INI);
        $this->assertEquals(IniFileAdapter::class, get_class($adapter));
    }

    public function testGetInvalidAdapter()
    {
        $this->expectException(ConfigException::class);
        $adapter = $this->getCoreObj()->getAdapter('invalid');
    }
}
