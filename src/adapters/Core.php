<?php

namespace harpya\config_manager\adapters;

use harpya\config_manager\Constants;
use harpya\config_manager\exceptions\ConfigException;

trait Core
{
    protected $adapters = [];

    protected function getAdapters(string $type)
    {
        switch ($type) {
            case Constants::ADAPTER_JSON:
                $className = JsonFileAdapter::class;
            break;
            case Constants::ADAPTER_ENV:
                $className = EnvFileAdapter::class;
            break;
            case Constants::ADAPTER_INI:
                $className = IniFileAdapter::class;
            break;
            case Constants::ADAPTER_YAML:
                $className = YamlFileAdapter::class;
            break;
            default:
                throw new ConfigException("Invalid type adapter $type", 1);
        }

        if (!isset($this->adapters[$className])) {
            $this->adapters[$className] = [];
        }
        $adapter = new $className($this);
        $this->adapters[$className][] = $adapter;
        return $adapter;
    }
}
