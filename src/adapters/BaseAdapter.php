<?php

namespace harpya\config_manager\adapters;

use harpya\config_manager\ConfigManager;
use harpya\config_manager\exceptions\ConfigException;

class BaseAdapter
{
    /**
     * @var ConfigManager
     */
    protected $context;


    /**
     * @return ConfigManager
     */
    public function getContext() : ConfigManager
    {
        return $this->context;
    }


    public function __construct($context)
    {
        // TODO: test if $context is a different class
        $this->context = $context;
    }

    protected function checkFileExists($fileName)
    {
        if (!file_exists($fileName)) {
            throw new ConfigException("File $fileName does not exists");
        }
    }
}
