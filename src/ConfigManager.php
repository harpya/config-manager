<?php

namespace harpya\config_manager;

use harpya\config_manager\adapters\Core;
use harpya\config_manager\exceptions\ConfigException;

/**
 * Class ConfigManager
 * @package harpya\config_manager
 */
class ConfigManager
{
    use Core;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param $key
     * @param $default
     * @return mixed
     */
    public function get($key, $default=null)
    {
        if (!$key) {
            throw new exceptions\ConfigException("Invalid key value");
        }

        switch (\gettype($key)) {
            case Constants::TYPE_ARRAY:
                return $this->getByArrayKeys($key, $default);
                break;
            default:
                if (isset($this->data[$key])) {
                    return $this->data[$key];
                }
        }

        return $default;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function set($key, $value)
    {
        switch (\gettype($key)) {
            case Constants::TYPE_ARRAY:
                $this->setByArrayKeys($key, $value);
                break;
            default:
                $this->data[$key] = $value;
        }

        return $this;
    }

    /**
     * @param string $filename
     */
    public function loadJSON(string $filename)
    {
        $this->getAdapters(Constants::ADAPTER_JSON)->load($filename);
    }

    /**
     * @param string $filename
     */
    public function loadEnv(string $filename)
    {
        $this->getAdapters(Constants::ADAPTER_ENV)->load($filename);
    }

    /**
     * @param string $filename
     */
    public function loadIni(string $filename)
    {
        $this->getAdapters(Constants::ADAPTER_INI)->load($filename);
    }

    /**
     * @param string $folderName
     * @param string $pattern
     * @throws ConfigException
     */
    public function loadEnvFolder(string $folderName, string $pattern='.*')
    {
        $list = $this->getFolderFiles($folderName, $pattern);
        foreach ($list as $filename) {
            $this->loadEnv($filename);
        }
    }

    /**
     * @param string $folderName
     * @param string $pattern
     * @throws ConfigException
     */
    public function loadIniFolder(string $folderName, string $pattern='*.ini')
    {
        $list = $this->getFolderFiles($folderName, $pattern);
        foreach ($list as $filename) {
            $this->loadIni($filename);
        }
    }


    /**
     * @param string $folderName
     * @param string $pattern
     * @throws ConfigException
     */
    public function loadJsonFolder(string $folderName, string $pattern='*.json')
    {
        $list = $this->getFolderFiles($folderName, $pattern);
        foreach ($list as $filename) {
            $this->loadJSON($filename);
        }
    }

    /**
     * @param string $folderName
     * @param string $pattern
     * @throws ConfigException
     */
    public function loadYamlFolder(string $folderName, string $pattern='*.yaml')
    {
        $list = $this->getFolderFiles($folderName, $pattern);
        foreach ($list as $filename) {
            $this->loadYaml($filename);
        }
    }

    /**
     * @param $folderName
     * @param string $pattern
     * @return array
     * @throws ConfigException
     */
    protected function getFolderFiles($folderName, $pattern='*')
    {
        $list = [];
        if (is_array($folderName)) {
            foreach ($folderName as $item) {
                $result = $this->getFolderFiles($item, $pattern);
                if (is_array($result) && !empty($result)) {
                    $list = array_merge($list, $result);
                }
            }
        } elseif (is_string($folderName)) {
            if (substr($folderName, -1) != '/') {
                $folderName .= '/';
            }
            if (!file_exists($folderName)) {
                throw new ConfigException("Folder $folderName does not exists.");
            }

            $result = glob($folderName.$pattern);
            if (is_array($result) && !empty($result)) {
                foreach ($result as $item) {
                    if (file_exists($item) && !is_dir($item)) {
                        $list[] = $item;
                    }
                }
            }
        }
        return $list;
    }


    /**
     * @param string $filename
     */
    public function loadYaml(string $filename)
    {
        $this->getAdapters(Constants::ADAPTER_YAML)->load($filename);
    }


    /**
     * @param array $arr
     */
    public function mergeConfig(array $arr=[])
    {
        $data = $this->data;
        $this->data = array_replace_recursive($data, $arr);
    }


    /**
     * @return array
     */
    public function getAll() {
        return $this->data;
    }

    //
    // ==================================
    // === Protected functions
    // ========
    //

    /**
     * @param $keys
     * @param $default
     */
    protected function getByArrayKeys($keys, $default=null)
    {
        $ptr = &$this->data;
        foreach ($keys as $key) {
            if (!isset($ptr[$key])) {
                return $default;
            }
            $ptr = &$ptr[$key];
        }
        return $ptr;
    }

    /**
     * @param $keys
     * @param $value
     */
    protected function setByArrayKeys($keys, $value)
    {
        $ptr = &$this->data;
        foreach ($keys as $key) {
            if (!isset($ptr[$key])) {
                $ptr[$key] = [];
            }
            $ptr = &$ptr[$key];
        }
        $ptr = $value;
    }

}
