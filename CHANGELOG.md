# Config Manager changelog

## Summary

- v0.0.1 - 2020-02-23;

## Versions


### v0.0.3
Date: 2020-03-03
- Added README.md
- Removed the Sentry as dependency.

### v0.0.2
Date: 2020-02-24
- Added YAML file adapter
- Added license on composer.json file

### v0.0.1
Date: 2020-02-23
Changes:
- Initial version
- Added initial infrastructure (composer files, tests, etc)
- Core functionalities
- JSON file format loader
- ENV file format loader
- INI file format loader
