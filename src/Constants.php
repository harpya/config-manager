<?php

namespace harpya\config_manager;

/**
 *
 */
class Constants
{
    const TYPE_ARRAY = 'array';

    const ADAPTER_JSON = 'json';
    const ADAPTER_ENV = 'env';
    const ADAPTER_INI = 'ini';
    const ADAPTER_YAML = 'yaml';
}
