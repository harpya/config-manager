<?php

namespace harpya\config_manager\adapters;

use Dotenv\Dotenv;
use harpya\config_manager\exceptions\ConfigException;
use Retrinko\Ini\IniFile;

class IniFileAdapter extends BaseAdapter
{

    /**
     *
     */
    public function load(string $fileName)
    {
        $this->checkFileExists($fileName);

        $ini = Inifile::load($fileName);
        $arr = $ini->toArray();

        if (!is_array($arr)) {
            throw new ConfigException("Invalid Array contents in $fileName");
        }

        $this->getContext()->mergeConfig($arr);
    }
}
