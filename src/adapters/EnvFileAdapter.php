<?php

namespace harpya\config_manager\adapters;

use Dotenv\Dotenv;
use harpya\config_manager\exceptions\ConfigException;

class EnvFileAdapter extends BaseAdapter
{

    /**
     *
     */
    public function load(string $fileName)
    {
        $this->checkFileExists($fileName);
        
        $original = $_ENV;
    
        $dotenv = Dotenv::createMutable(dirname($fileName), basename($fileName));
        $dotenv->load();

        $arr = $_ENV;

        $_ENV = $original;

        if (!is_array($arr)) {
            throw new ConfigException("Invalid Array contents in $fileName");
        }

        $this->getContext()->mergeConfig($arr);
    }
}
