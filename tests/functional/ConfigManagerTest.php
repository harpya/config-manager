<?php

use PHPUnit\Framework\TestCase;
use \harpya\config_manager\ConfigManager;

class ConfigManagerTest extends TestCase
{
    public function testGetEmptyConfigGet()
    {
        $cfg = new ConfigManager();
        $this->expectException(\harpya\config_manager\exceptions\ConfigException::class);
        $this->expectExceptionMessageRegExp('/Invalid key/');
        $cfg->get(false);
    }

    public function testGetEmptyConfig()
    {
        $cfg = new ConfigManager();
        $return = $cfg->get('empty', 1);
        $this->assertEquals(1, $return);
    }

    public function testSetGetConfig()
    {
        $cfg = new ConfigManager();
        $cfg->set('value', 15);
        $return = $cfg->get('value');
        $this->assertEquals(15, $return);
    }

    public function testSetArray()
    {
        $cfg = new ConfigManager();
        $cfg->set(['admin','email'], 'test@test.com');
        $return = $cfg->get(['admin','email']);
        $this->assertEquals('test@test.com', $return);
    }

    public function testSetArrayAlreadyExistent()
    {
        $cfg = new ConfigManager();
        $cfg->set('admin', ['email'=>'x@abc.com', 'name'=>'Eduardo']);
        $cfg->set(['admin','email'], 'test@test.com');
        $this->assertIsArray($cfg->get('admin'));
        $email = $cfg->get(['admin','email']);
        $this->assertEquals('test@test.com', $email);
        $this->assertArrayHasKey('name', $cfg->get('admin'));
        $this->assertEquals('Eduardo', $cfg->get(['admin','name']));
    }

    public function testLoadJSONExceptionFileDoesNotExists()
    {
        $cfg = new ConfigManager();

        $this->expectException(\harpya\config_manager\exceptions\ConfigException::class);
        $this->expectExceptionMessageRegExp('/File inexistent.json does not exists/');
        $cfg->loadJSON('inexistent.json');
    }

    public function testLoadJSONInvalidContents()
    {
        $cfg = new ConfigManager();
        $this->expectException(\harpya\config_manager\exceptions\ConfigException::class);
        $this->expectExceptionMessageRegExp('/Invalid JSON contents in/');
        $cfg->loadJSON(__DIR__.'/files/invalid.json');
    }

    public function testLoadJSONValid()
    {
        $cfg = new ConfigManager();
        $cfg->set('key', 'empty');
        $cfg->loadJSON(__DIR__.'/files/valid.json');
        $this->assertEquals('value', $cfg->get('key', false));
        $this->assertEquals(false, $cfg->get('inexistent-key', false));
    }

    public function testLoadEnvFileDoesNotExists()
    {
        $cfg = new ConfigManager();
        $this->expectException(\harpya\config_manager\exceptions\ConfigException::class);
        $this->expectExceptionMessageRegExp('/File .env.inexistent does not exists/');

        $cfg->loadEnv('.env.inexistent');
    }

    public function testLoadEnvValid()
    {
        $cfg = new ConfigManager();
        $cfg->set('key', 'empty');
        $cfg->loadEnv(__DIR__.'/files/.env.simple');
        $this->assertEquals('app_name', $cfg->get('APPLICATION', false));
        $this->assertEquals($cfg->get('ALIAS'), $cfg->get('APPLICATION'));
        $this->assertEquals(false, $cfg->get('inexistent-key', false));
    }

    public function testLoadIniFileDoesNotExists()
    {
        $cfg = new ConfigManager();
        $this->expectException(\harpya\config_manager\exceptions\ConfigException::class);
        $this->expectExceptionMessageRegExp('/File inexistent.ini does not exists/');
        $cfg->loadIni('inexistent.ini');
    }

    public function testLoadIniValid()
    {
        $cfg = new ConfigManager();
        $cfg->set('KEY', 'empty');
        $cfg->loadIni(__DIR__.'/files/simple.ini');
        $this->assertEquals('VALUE', $cfg->get(['default', 'KEY'], false));
        $this->assertEquals(false, $cfg->get('inexistent-key', false));
    }

    public function testLoadYamlFileDoesNotExists()
    {
        $cfg = new ConfigManager();
        $this->expectException(\harpya\config_manager\exceptions\ConfigException::class);
        $this->expectExceptionMessageRegExp('/File (.*)inexistent.yaml does not exists/');
        $cfg->loadYaml(__DIR__.'/files/inexistent.yaml');
    }

    public function testLoadYamlValid()
    {
        $cfg = new ConfigManager();
        $cfg->set(['level1','KEY'], 'empty');
        $cfg->loadYaml(__DIR__.'/files/simple.yaml');
        $this->assertEquals('VALUE', $cfg->get(['level1','KEY'], false));
        $this->assertEquals(false, $cfg->get('inexistent-key', false));
    }
}
